package be.cm.apps.playground.ee7minimal;

import java.io.IOException;
import java.security.Principal;

import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * The Java class will be hosted at the URI path "/hello", for example: http://localhost:8080/playground-ee7-web/secure.
 * 
 * It demonstrates how to secure a servlet defined with only annotations.
 * 
 * @author Ivan Belis
 *
 */
@SuppressWarnings("serial")
@WebServlet(urlPatterns = "/secure",
			name = "SecureHelloWorldServlet",
			displayName = "Hello Java EE 7",
			description = "Example EE 7 servlet")
@WebInitParam(name = "foo", value = "Hello ")
@ServletSecurity(value = @HttpConstraint( rolesAllowed="adminRole"))
public class SecureHelloWorldServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		Principal user = req.getUserPrincipal();
		resp.getWriter().printf("Hello secure World! (user=%s)\n", (user != null) ? user.getName() : user);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doGet(req,resp);
	}

}