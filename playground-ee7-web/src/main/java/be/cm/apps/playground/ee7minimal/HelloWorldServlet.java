package be.cm.apps.playground.ee7minimal;

import java.io.IOException;
import java.security.Principal;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * The Java class will be hosted at the URI path "/hello", for example: http://localhost:8080/playground-ee7-web/hello.
 * 
 * It demonstrates a servlet defined with only annotations.
 * 
 * @author Ivan Belis
 *
 */
@SuppressWarnings("serial")
@WebServlet(urlPatterns = "/hello", name = "HelloWorldServlet", displayName = "Hello Java EE 7 Servlet", description = "Example EE 7 servlet", initParams = {
		@WebInitParam(name = "foo", value = "Hello ")})
public class HelloWorldServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		Principal user = req.getUserPrincipal();
		resp.getWriter().printf("Hello World! (user=%s)\n", (user != null) ? user.getName() : user);

	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doGet(req, resp);
	}

}