playground-ee7-web
==============

Java EE 7 example of a secure web application.

Demonstrates
-------------------------
Using only annotations to define the Servlet.
Setting security constraints on a servlet.

Testing
========

To compile the entire project, run "mvn install".

Deploy to a web server (servlet 3+) for example TomEE.

http://localhost:8080/playground-ee7-web/hello

Testing on Weblogic
-------------------------
mvn install -Plocal-deploy

http://localhost:7001/playground-ee7-web-0.0.1-SNAPSHOT/hello
http://localhost:7001/playground-ee7-web-0.0.1-SNAPSHOT/secure